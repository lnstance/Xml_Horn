﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Linq;
using System.Xml.Linq;
using Imperialism;







namespace Xml_Horn
{
    public partial class Window : Form
    {

        iCore Imp = new iCore();
        XDocument Xml_File = null;
        XDocument Info_File = null;


        bool Hovering = false;
        bool User_Input = false;
        bool Show_Tooltip = true;
        string Temporal_A, Temporal_B = "";
        int Temporal_C = 0;
        int Temporal_D = 0;

        string Version = " 0.1";
        int Last_Appended = 0;
        string Faction_Directory = "";
        string Output_Folder = Properties.Settings.Default.Output_Folder;
        string Input_Folder = Properties.Settings.Default.Input_Folder;


        // Tag_Names.Add("Build_Started", "SFXEvent_Build_Started");
        IDictionary<string, string> Tag_Alias = new Dictionary<string, string>()             
        {   {"Build_Started", "Move_Into_Asteroid_Field"},
	        {"Build_Cancelled", "SFXEvent_Move_Into_Nebula"},
	        {"Build_Completed", "SFXEvent_Build_Complete"},
            {"Spawn", "SFXEvent_Build_Complete"},    
          
            {"Bombard_Aim", "SFXEvent_Bombard_Select_Target"},        
            {"HP_Destroyed", "SFXEvent_Hardpoint_Destroyed"},
            {"Health_Low", "SFXEvent_Health_Low_Warning"},
	        {"Health_Damaged", "SFXEvent_Enemy_Damaged_Health_Low_Warning"},
	        {"Health_Critical", "SFXEvent_Enemy_Damaged_Health_Critical_Warning"},  
            {"Die", "Death_SFXEvent_Start_Die"},

            {"Engine_Idle", "SFXEvent_Engine_Idle_Loop"},
	        {"Engine_Moving", "SFXEvent_Engine_Moving_Loop"},
	               
            {"Ability_Activated", "SFXEvent_GUI_Unit_Ability_Activated"},
	        {"Ability_Deactivated", "SFXEvent_GUI_Unit_Ability_Deactivated"},                
        };



        public Window()
        {

            InitializeComponent();
  

            this.Text += Version;

            Drop_Zone.AllowDrop = true;
            Text_Box_Folder_Path.AllowDrop = true;
            Text_Box_Output_Path.AllowDrop = true;

            Text_Box_Folder_Path.Text = Input_Folder;
            Text_Box_Output_Path.Text = Output_Folder;

            Combo_Box_Language.Text = Properties.Settings.Default.Language;
            Combo_Box_Path_Prefix.Text = @"data\audio\"; // Properties.Settings.Default.Path_Prefix;  // Defaulting into this instead
            Combo_Box_Presets.Text = "Preset_UR"; // Properties.Settings.Default.Last_Preset; // Disabled because Preset_UR is by far the most used.
            Text_Box_Chained_Event.Text = Properties.Settings.Default.Chained_Event;


            Check_Box_Add_To_File.Checked = Properties.Settings.Default.Add_To_File;
            Check_Box_Merge_Code.Checked = Properties.Settings.Default.Merge_Code;
            Check_Box_Localize.Checked = Properties.Settings.Default.Localize;
            Check_Box_Sequencially.Checked = Properties.Settings.Default.Sequencially;
            Check_Box_Overlap_Test.Checked = Properties.Settings.Default.Overlap_Test;


            Track_Bar_Volume.Value = Properties.Settings.Default.Volume;
            Track_Bar_Pitch.Value = Properties.Settings.Default.Pitch;
            Track_Bar_Probability.Value = Properties.Settings.Default.Delay;                 
            Track_Bar_Volume_Scroll(null, null);
            Track_Bar_Pitch_Scroll(null, null);
            Track_Bar_Probability_Scroll(null, null);
        

            Button_Browse_Folder_MouseLeave(null, null);
            Button_Xml_Folder_MouseLeave(null, null);
            Button_Dispatch_MouseLeave(null, null);

            Control[] Controls = { Button_Xml_Folder, Button_Browse_Folder, Button_Dispatch };
            foreach (Control Selectrion in Controls) { Selectrion.BackColor = Color.Transparent; }   

               

            User_Input = true;
        }





        //-----------------------------------------------------------------------------
        // Enabling Drag and Drop
        //-----------------------------------------------------------------------------

        // Don't forget about Drag_And_Drop_Area.AllowDrop = true; in the constructor!
        private void Drop_Zone_DragDrop(object sender, DragEventArgs e)
        {
            var Data = e.Data.GetData(DataFormats.FileDrop);
            if (Data != null)
            {
                var File_Names = Data as string[];
                if (File_Names.Length > 0)
                {
                    // string Image_Name = Path.GetFileName(File_Names[0]);                    
                    try // Just to be on the safe side
                    {
                        if (!File.GetAttributes(File_Names[0]).HasFlag(FileAttributes.Directory)) // Check whether it is a file or a directory                                                     
                        { MessageBox.Show("\nError: the file needs to either be a folder."); }
                        // else { Set_Paths(File_Names[0]); }
                        else { Set_Paths(Text_Box_Folder_Path, File_Names[0]); }

                    }
                    catch { }
                }
            }

        }

        private void Drop_Zone_DragEnter(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Copy; // This means the dragged Map images        
        }

        private void Drop_Zone_DragOver(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Move; }


        private void Drop_Zone_Click(object sender, EventArgs e)
        { Disable_Description(); } // Hiding






        private void Text_Box_Folder_Path_DragDrop(object sender, DragEventArgs e)
        {
            var Data = e.Data.GetData(DataFormats.FileDrop);
            if (Data != null)
            {
                var File_Names = Data as string[];
                if (File_Names.Length > 0)
                {
                    // string Image_Name = Path.GetFileName(File_Names[0]);                    
                    try // Just to be on the safe side
                    {
                        if (!File.GetAttributes(File_Names[0]).HasFlag(FileAttributes.Directory)) // Check whether it is a file or a directory                                                     
                        { MessageBox.Show("\nError: the file needs to either be a folder."); }
                        // else { Set_Paths(File_Names[0]); }
                        else { Set_Paths(Text_Box_Folder_Path, File_Names[0]); }

                    }
                    catch { }
                }
            }
        }

        private void Text_Box_Folder_Path_DragEnter(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Copy; }

        private void Text_Box_Folder_Path_DragOver(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Move; }





        private void Text_Box_Output_Path_DragDrop(object sender, DragEventArgs e)
        {
            var Data = e.Data.GetData(DataFormats.FileDrop);
            if (Data != null)
            {
                var File_Names = Data as string[];
                if (File_Names.Length > 0)
                {
                    // string Image_Name = Path.GetFileName(File_Names[0]);                    
                    try // Just to be on the safe side
                    {
                        if (!File.GetAttributes(File_Names[0]).HasFlag(FileAttributes.Directory)) // Check whether it is a file or a directory                                                     
                        { MessageBox.Show("\nError: the file needs to either be a folder."); }
                        // else { Set_Paths(File_Names[0]); }
                        else { Set_Paths(Text_Box_Output_Path, File_Names[0]); }

                    }
                    catch { }
                }
            }
        }

        private void Text_Box_Output_Path_DragEnter(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Copy; }

        private void Text_Box_Output_Path_DragOver(object sender, DragEventArgs e)
        { e.Effect = DragDropEffects.Move; }





        public void Set_Paths(TextBox The_Text_Box, string The_Path)
        {
            if (The_Path != "" && The_Path != null)
            { 
                The_Text_Box.Text = The_Path;
                if (!The_Text_Box.Text.EndsWith(@"\")) { The_Text_Box.Text += @"\"; }

                if (The_Text_Box == Text_Box_Folder_Path) { Properties.Settings.Default.Input_Folder = The_Text_Box.Text; }
                else 
                { 
                    Output_Folder = The_Path;
                    if (!Output_Folder.EndsWith(@"\")) { Output_Folder += @"\"; }
                    Properties.Settings.Default.Output_Folder = Output_Folder;
                }
                Properties.Settings.Default.Save();

            }

        }


        //===========================//

        private void Text_Box_Folder_Path_TextChanged(object sender, EventArgs e)
        {

        }

        //===========================//

        private void Text_Box_Description_Click(object sender, EventArgs e)
        { Disable_Description();  }


        //===========================//

        private void Disable_Description()
        {
            Text_Box_Description.Text = "";
            Text_Box_Description.Visible = false;
        }

        //===========================//

        private void Description(string New_Text = "", bool In_Black = true)
        {
            Text_Box_Description.Text = New_Text;
            Text_Box_Description.Visible = true;

            //if (In_Black) { Text_Box_Description.BackColor = Color.Black; } todo
            //else { Text_Box_Description.BackColor = Theme_Color; }
        }




        //===========================//

        private void Inject_To_File(List<string> File_List)
        {
           string Event_File = Output_Folder + @"SFXEventFiles.xml";
           if (!File.Exists(Event_File)) { MessageBox.Show("   Can't find SFXEventFiles.xml in the Output Folder."); return; }

           XDocument SFXEventFiles = XDocument.Load(Event_File, LoadOptions.PreserveWhitespace);

           /*
           IEnumerable<XElement> File_Entries =
                 from All_Tags in SFXEventFiles.Root.Descendants()
                 where All_Tags.Name == "File"
                 select All_Tags;
            */

         
           File_List.Reverse();

           foreach (string File_Name in File_List)
           {
               // string Entry = Faction_Name + @"\" + File_Path.Substring(Output_Folder.Length);
               // string Entry = Faction_Name + @"\" + Path.GetFileName(File_Path);
               
               string Entry = Faction_Directory + @"\" + File_Name;
               if (Faction_Directory == Path.GetFileNameWithoutExtension(File_Name)) { Entry = File_Name; } // Fallback

               if (!SFXEventFiles.Root.Descendants("File").Any(x => x.Value == Entry)) // Preventing Duplicates
               { SFXEventFiles.Root.Descendants().First().AddAfterSelf("\n\t", new XElement("File", Entry)); }
           }

           SFXEventFiles.Save(Event_File);

        }


        //===========================//

        private void Button_Run_Click(object sender, EventArgs e)
        {          
            bool At_Top_Level = true; // Assumed     
            bool Create_File = true;
            string First_File = "";
            string Last_Unit_Name = "";         
            List<string> File_List = new List<string>();

            Create_Xml("SFX_Info");

            string[] Actions = new string[] { "select", "move", "attack", "destroyed", "die", "ability" };

         
            foreach (string Folder_Path in Imp.Get_All_Directories(Properties.Settings.Default.Input_Folder, true))
            {              
                // MessageBox.Show(string.Join("\n", Found_Wav_Files)); return;             
                string Root_Action_Name = Path.GetFileName(Folder_Path);
                string Root_Unit_Name = Path.GetFileName(Path.GetDirectoryName(Folder_Path)).Replace(" ", "_");
       
                foreach (string Action in Actions)
                {   // No action directories detected in this directory, means we're still somewhere higher in the path
                    if (Root_Action_Name.ToLower() == Action) { At_Top_Level = false; break; }
                }

                if (Create_File) { Clear_Xml_File("SFXEvent"); }




                if (!At_Top_Level && Root_Unit_Name != "")
                {
                    Process_Sound(Folder_Path, At_Top_Level); Create_File = false;
                    First_File = Output_Folder + Root_Unit_Name + ".xml";
                }
                else
                {
                    string Unit_Name = "";
                  
                    foreach (string Unit_Folder in Imp.Get_All_Directories(Folder_Path, true))
                    {
                        Unit_Name = Path.GetFileName(Path.GetDirectoryName(Unit_Folder)).Replace(" ", "_");
                        if (Unit_Name == "" || Unit_Name == "Template") { continue; }

                        if (Last_Unit_Name != Unit_Name)
                        {
                            Info_File.Descendants("SFX_Infos").First().Add("\n\n\n\t",
                              new XComment(" ================================================================================ "), "\n\t",
                              new XComment(" ***********************\t" + Unit_Name + "\t*********************** "), "\n\t",
                              new XComment(" ================================================================================ ")//, "\n"
                            );

                            Last_Unit_Name = Unit_Name;
                        }

                        Process_Sound(Unit_Folder);                          
                    }


                    if (Create_File && Unit_Name != "" && Unit_Name != "Template")
                    {
                        

                        if (Check_Box_Merge_Code.Checked)
                        {
                            Create_File = false;  // Stop, once first one was created
                            string Root_Faction_Name = Path.GetFileName(Path.GetDirectoryName(Folder_Path));
                            First_File = Output_Folder + Root_Faction_Name + ".xml";
                        }

                        else
                        {   File_List.Add(Unit_Name + ".xml");

                            if (Faction_Directory == "") { Xml_File.Save(Output_Folder + Unit_Name + ".xml"); }
                            else
                            {
                                if (!Directory.Exists(Output_Folder + Faction_Directory)) { Directory.CreateDirectory(Output_Folder + Faction_Directory); }
                                Xml_File.Save(Output_Folder + Faction_Directory + @"\" + Unit_Name + ".xml");
                            }
                        }
                    }

                    Root_Unit_Name = Unit_Name;
                }


                Last_Appended = 0; // Reset                     
            }


            if (First_File != "") { Xml_File.Save(First_File); File_List.Add(Path.GetFileName(First_File)); }


            Info_File.Save(Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Output_Folder))) + @"\Info_File.xml"); 
            // if (Faction_Directory == "") { Info_File.Save(Output_Folder + "Info_File.xml"); } // Too many different locations
            // else { Info_File.Save(Output_Folder + Faction_Directory + @"\" + "Info_File.xml"); }

            if (Check_Box_Add_To_File.Checked) { Inject_To_File(File_List); } 
            Faction_Directory = ""; // Clear
        }



        //===========================//

        private void Process_Sound(string Unit_Folder, bool At_Top_Level = true) 
        {
            List<string> Found_Wav_Files = Imp.Get_All_Files(Unit_Folder, "wav");
            if (Found_Wav_Files == null || Found_Wav_Files.Count == 0) { return; }
            // string Unit_Folder = Target_Path.Replace(" ", "_"); // Get rid of illegal emptyspace characters

          
            string Cue_Name = "";
            // The expected folder structure is :  data/audio/faction/unit/actions/file_name.wav
            string Action_Name = Path.GetFileName(Unit_Folder);
            string Unit_Name = Path.GetFileName(Path.GetDirectoryName(Unit_Folder)).Replace(" ", "_");
            if (Unit_Name == "" || Unit_Name == "Template") { return; }
            Faction_Directory = Path.GetFileName(Path.GetDirectoryName(Path.GetDirectoryName(Unit_Folder)));


            string Path_To_Faction = "";
            if (At_Top_Level) { Path_To_Faction = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Unit_Folder))); }
            else { Path_To_Faction = Path.GetDirectoryName(Path.GetDirectoryName(Unit_Folder)); }

         

            List<string> Cue_Names = new List<string>();
            string[] The_Extensions = new string[] { "ENG", "Eng", "eng", "GER", "Ger", "ger", "FR", "Fr", "fr" };
            

            foreach (string File_Path in Found_Wav_Files)
            {   // Assamble path and get rid of illegal emptyspace characters
                Cue_Name = Combo_Box_Path_Prefix.Text + File_Path.Substring(Path_To_Faction.Length + 1).Replace(" ", "_"); // +1 because of the \ sign
                if (Cue_Name == Combo_Box_Path_Prefix.Text) { continue; }


                foreach (string Extension in The_Extensions)
                {   // Remove extensions
                    if (Cue_Name.Contains("_" + Extension)) { Cue_Name = Cue_Name.Replace("_" + Extension, ""); break; }
                }

                if (!Cue_Names.Contains(Cue_Name)) { Cue_Names.Add(Cue_Name); }
            }

            // Parameter 3 is supposed to be the folder name.
            Add_Sound("SFXEvent", "Name", Unit_Name, Action_Name, Cue_Names);
        }



        //===========================//
    
        private void Clear_Xml_File(string Entity_Name)
        {
            if (Entity_Name == "" || Entity_Name == "Template") { return; }


            Xml_File =

            new XDocument
            ("\n\n\n",
                 new XElement(Entity_Name + "s"), "\n\n" // Root_Name                                           
            );

            // Xml_File.Save(@"C:\Users\Mod\Desktop\Python\test.xml"); // Obsolete
        }


        //===========================//

        private void Create_Xml(string Entity_Name)
        {         
            Info_File =

            new XDocument
            ("\n\n\n",
                 new XElement(Entity_Name + "s"), "\n\n" // Root_Name                                           
            );
        }



        //===========================//

        // Create_Xml("SFXEvents", "SFXEvent", "Name", "Dies")
        private void Add_Sound(string Entity_Name, string Attribute_Name, string Unit_Name, string Action_Name, List<string> Cue_Names)
        {
            string Localize = "No";
            string Play_Sequentially = "No";
            string Attribute_Value = Unit_Name + "_" + Action_Name;

            if (Check_Box_Localize.Checked) { Localize = "Yes"; }
            if (Check_Box_Sequencially.Checked) { Play_Sequentially = "Yes"; }



            int Cue_Lines = 1;
            int Text_Lines = 1;
            string One = "\n\t";
            string Two = "\n\t\t";
            string Three = "\n\t\t\t";
          
            string Cues = "";
            string Text_NONE = "TEXT_SFX_NONE ";
            string Text_IDS = "";


  
            for (int i = Cue_Names.Count(); i > 0; --i)         
            {   try
                {
                    if (Cues.Length > 80 * Cue_Lines) { Cues += Three; Cue_Lines++; } // Add new Lines
                    Cues += Cue_Names[i - 1] + " ";

                    if (Text_IDS.Length > 80 * Text_Lines) { Text_IDS += Three; Text_Lines++; }
                    Text_IDS += Text_NONE;

                    if (i == 1 && Text_Lines > 1) { Cues += Two; Text_IDS += Two; Text_Lines++; } // New line for the closing tag
                } catch {}
            }


            try
            {
                string Tabs = Two;
                if (Last_Appended == 0) { Tabs = One; }


                string Tag_Name = "SFXEvent_" + Action_Name;
                if (Tag_Alias.ContainsKey(Action_Name)) { Tag_Name = Tag_Alias[Action_Name]; }
               

                Info_File.Descendants("SFX_Infos").First().Add(One,
                    new XElement(Tag_Name, Attribute_Value)
                );



                Xml_File.Descendants(Entity_Name + "s").First().Add(

                    new XElement(Entity_Name, new XAttribute(Attribute_Name, Attribute_Value), Two,
                        new XElement("Use_Preset", Combo_Box_Presets.Text), Two,
                        new XElement("Localize", Localize), Two,
                        new XElement("Play_Sequentially", Play_Sequentially), Two,
                     
                        // new XElement("Kills_Previous_Object_SFX", "Yes"), Two, 
                        // new XElement("Max_Instances", "10"), Two, // Used with Preset_EXPC
                      
                        
                        new XElement("Samples", Cues), Two,
                        new XElement("Text_ID", Text_IDS), Tabs // Text_Box_Text_ID.Text.Replace("\n", ", "))
                     ), "\n\n\n\t"
                );

            

                if (Check_Box_Overlap_Test.Checked)
                {
                    if (Last_Appended == 0) { Last_Appended = 1; }
                    if (Last_Appended == 1) { Tabs = One; }

                    Xml_File.Descendants(Entity_Name).Last().Add(
                        new XElement("Overlap_Test", Unit_Name), Tabs // I believe this tests whether SFX overlap for this ingame unit.                     
                    );                  
                }

                if (Text_Box_Chained_Event.Text != "")
                {
                    if (Last_Appended < 2) { Last_Appended = 2; }
                    if (Last_Appended == 2) { Tabs = One; }

                    Xml_File.Descendants(Entity_Name).Last().Add(
                        new XElement("Chained_SFXEvent", Text_Box_Chained_Event.Text), Tabs
                    );
                }


                if (Text_Box_Min_Volume.Text != "" && Text_Box_Min_Volume.Text != "0"
                    || Text_Box_Max_Volume.Text != "" && Text_Box_Max_Volume.Text != "0")
                {   // Shall be the newest   "\n\t"

                    if (Last_Appended < 3) { Last_Appended = 3; } 
                    if (Last_Appended == 3) { Tabs = One; }

                    Xml_File.Descendants(Entity_Name).Last().Add(
                        new XElement("Min_Volume", Text_Box_Min_Volume.Text), Two,
                        new XElement("Max_Volume", Text_Box_Max_Volume.Text), Tabs
                    );
                }


                if (Text_Box_Min_Pitch.Text != "" && Text_Box_Min_Pitch.Text != "0"
                  || Text_Box_Max_Pitch.Text != "" && Text_Box_Max_Pitch.Text != "0")
                {
                    if (Last_Appended < 4) { Last_Appended = 4; }
                    if (Last_Appended == 4) { Tabs = One; }

                    Xml_File.Descendants(Entity_Name).Last().Add(
                        new XElement("Min_Pitch", Text_Box_Min_Pitch.Text), Two,
                        new XElement("Max_Pitch", Text_Box_Max_Pitch.Text), Tabs
                    );
                }




                if (Combo_Box_Presets.Text == "Preset_GUI" || Combo_Box_Presets.Text == "Preset_HUD")
                {
                    if (Text_Box_Min_Probability.Text != "" && Text_Box_Min_Probability.Text != "0"
                        || Text_Box_Max_Probability.Text != "" && Text_Box_Max_Probability.Text != "0")
                    {
                        if (Last_Appended < 5) { Last_Appended = 5; }
                        if (Last_Appended == 5) { Tabs = One; }


                        Xml_File.Descendants(Entity_Name).Last().Add(
                            new XElement("Min_Predelay", Text_Box_Min_Probability.Text), Two,// In Milliseconds
                            new XElement("Max_Predelay", Text_Box_Max_Probability.Text), Tabs
                        );
                    }
                }
                else if (Text_Box_Max_Probability.Text != "" && Text_Box_Max_Probability.Text != "0")
                {
                    if (Last_Appended < 5) { Last_Appended = 5; }
                    if (Last_Appended == 5) { Tabs = One; }

                    Xml_File.Descendants(Entity_Name).Last().Add(
                        new XElement("Probability", Text_Box_Max_Probability.Text), Tabs
                    );                   
                }



            } catch {}  



        }





        // Create_Xml("SFXEvents", "SFXEvent", "Name", "Dies")
        private void Create_Xml2(string Entity_Name, string Attribute_Name, string Attribute_Value)
        { 
            string Localize = "No";
            string Play_Sequentially = "No";

            if (Check_Box_Localize.Checked) { Localize = "Yes"; }
            if (Check_Box_Sequencially.Checked) { Play_Sequentially = "Yes"; }



            Xml_File =

            new XDocument
            ("\n\n\n",

                 new XElement(Entity_Name + "s", // Root_Name,

                     new XElement(Entity_Name, new XAttribute(Attribute_Name, Attribute_Value),
                        new XElement("Use_Preset", Combo_Box_Presets.Text), // Todo
                        new XElement("Localize", Localize),
                        new XElement("Play_Sequentially", Play_Sequentially),
                        new XElement("Text_ID", Text_Box_Text_ID.Text.Replace("\n", ", ")),
                        new XElement("Overlap_Test", ""),

                        new XElement("Min_Predelay", ""), // In Milliseconds
                        new XElement("Max_Predelay", ""),
                        new XElement("Samples", "2 3 4 5")

                     ), "\n\n\n"


                 ), "\n\n"
            );

            // Xml_File.Save(@"C:\Users\Mod\Desktop\Python\test.xml");
        }



        private void Combo_Box_Language_TextChanged(object sender, EventArgs e)
        {
            if (!User_Input) { return; }
            Properties.Settings.Default.Language = Combo_Box_Language.Text;
            Properties.Settings.Default.Save();
        }

        private void Combo_Box_Path_Prefix_TextChanged(object sender, EventArgs e)
        {
             if (!User_Input) { return; }
             Properties.Settings.Default.Path_Prefix = Combo_Box_Path_Prefix.Text;
             Properties.Settings.Default.Save();          
        }

        private void Combo_Box_Presets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!User_Input) { return; }

            Properties.Settings.Default.Last_Preset = Combo_Box_Presets.Text;
            Properties.Settings.Default.Save();

            // Alias Modes
            if (Combo_Box_Presets.Text == "Space" || Combo_Box_Presets.Text == "Ground") { Combo_Box_Presets.Text = "Preset_UR"; return; }
            if (Combo_Box_Presets.Text == "Buildings") { Combo_Box_Presets.Text = "Preset_BDS"; return; }


            Clear_UI();

            switch (Combo_Box_Presets.Text)
            {
                case "Preset_UR":
                case "Preset_EXP":
                    Check_Box_Localize.Checked = true;
                    Check_Box_Sequencially.Checked = true;
                    Check_Box_Overlap_Test.Checked = true;
                    break;

                case "Preset_EXP_SR":
                case "Preset_EXP_LR":
                    Check_Box_Localize.Checked = true;
                    Check_Box_Sequencially.Checked = true;
                    Check_Box_Overlap_Test.Checked = true;
                    Track_Bar_Probability.Value = 9;
                    Track_Bar_Probability_Scroll(null, null);
                    break;

                case "Preset_BDS":                
                    Check_Box_Overlap_Test.Checked = true;
                    break;

                case "Preset_GUNS":
                case "Preset_GUNV": // Used very often
                case "Preset_EGB_Land":
                    Track_Bar_Volume.Value = 7;
                    Track_Bar_Volume_Scroll(null, null);
                    break;

                case "Preset_GUI":                           
                case "Preset_HUD":
                    Label_Probability.Text = "Predelay";
                    Text_Box_Min_Probability.Visible = true;

                    break;
                case "Preset_EGLC_Idle":
                    Track_Bar_Volume.Value = 5;
                    Track_Bar_Volume_Scroll(null, null);
                    Track_Bar_Pitch.Value = 5;
                    Track_Bar_Pitch_Scroll(null, null);
        
                    break;         
            }        
        }

        private void Clear_UI(bool Ignore_Checkboxes = false, bool Ignore_Trackbars = false)
        {
            Text_Box_Chained_Event.Text = "";

            if (!Ignore_Checkboxes)
            {   // Check_Box_Add_To_File.Checked = false;
                // Check_Box_Merge_Code.Checked = false;
                Check_Box_Localize.Checked = false;
                Check_Box_Sequencially.Checked = false;
                Check_Box_Overlap_Test.Checked = false;
            }

            if (!Ignore_Trackbars)
            {
                Label_Probability.Text = "Probability";
                Text_Box_Min_Probability.Visible = false;

                Track_Bar_Volume.Value = 0;
                Track_Bar_Pitch.Value = 0;
                Track_Bar_Probability.Value = 0;

                Control [] Text_Boxes = new Control[] {Text_Box_Min_Volume, Text_Box_Max_Volume, Text_Box_Min_Pitch, 
                    Text_Box_Max_Pitch, Text_Box_Min_Probability, Text_Box_Max_Probability};

                foreach (Control Box in Text_Boxes) { Box.Text = ""; }
            }
        }

        private void Text_Box_Chained_Event_TextChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Chained_Event = Text_Box_Chained_Event.Text;
            Properties.Settings.Default.Save();
        }


        private void Check_Box_Add_To_File_CheckedChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Add_To_File = Check_Box_Add_To_File.Checked;
            Properties.Settings.Default.Save();
        }

        private void Check_Box_Merge_Code_CheckedChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Merge_Code = Check_Box_Merge_Code.Checked;
            Properties.Settings.Default.Save();
        }

        private void Check_Box_Localize_CheckedChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Localize = Check_Box_Localize.Checked;
            Properties.Settings.Default.Save();
        }

        private void Check_Box_Sequencially_CheckedChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Sequencially = Check_Box_Sequencially.Checked;
            Properties.Settings.Default.Save();
        }

        private void Check_Box_Overlap_Test_CheckedChanged(object sender, EventArgs e)
        {   if (!User_Input) { return; }
            Properties.Settings.Default.Overlap_Test = Check_Box_Overlap_Test.Checked;
            Properties.Settings.Default.Save();
        }


        // Todo: This stub is prepared but has currently no function
        private void Show_Text_ID(bool Show = true)
        {   if (Show)
            {   Label_Language.Text = "Text IDs"; 
                Text_Box_Text_ID.Visible = true;
                List_View_Selection.Visible = true;
                Label_Path_Prefix.Visible = false;
            }
            else 
            {   Label_Language.Text = "Language"; 
                Text_Box_Text_ID.Visible = false;
                List_View_Selection.Visible = false;
                Label_Path_Prefix.Visible = true;
            }
        }




        public void Scroll_Xml_Value(TrackBar Track_Bar, ProgressBar Progress_Bar, TextBox Text_Box, int Factor = 1, int Maximal_Value = 100)
        {
            // Scrolling = true;

            // Fighters and Ground Units have a smaller maximum value, so we adjust the divisor number for such cases
            if (Maximal_Value < 999 & Factor == 100) { Factor = 10; }


            if (Progress_Bar != null)
            {
                Progress_Bar.Maximum = Maximal_Value / Factor;
                Track_Bar.Maximum = Progress_Bar.Maximum;
                Progress_Bar.Value = Track_Bar.Value;
            }

            // Setting Value of the Text Box, according to the Maximal Value 
            Text_Box.Text = (Track_Bar.Value * Factor).ToString();

            //Scrolling = false;
        }


        // Text_Box_Text_Changed(Track_Bar, Progress_Bar, Maximal_Value, Typed_Value, Number)
        public void Text_Box_Text_Changed(TrackBar Track_Bar, ProgressBar Progress_Bar, int Typed_Value, int Factor = 10, int Maximal_Value = 100)
        {
            // if (Scrolling) { return; }

            // Fighters and Ground Units have a smaller maximum value, so we adjust the divisor number for such cases
            if (Maximal_Value < 999 & Factor == 100) { Factor = 10; }

            // If the value is higher then Maximum we make sure the Bars are full
            if (Typed_Value > Maximal_Value)
            {
                if (Progress_Bar != null) { Progress_Bar.Value = Progress_Bar.Maximum; }
                Track_Bar.Value = Track_Bar.Maximum;
            }
            else
            {
                try
                {   // Setting amount in the Trackbar according to the Maximal Value 
                    Track_Bar.Maximum = Maximal_Value / Factor;
                    // Then we need to set the Track Bar according to the Text box / 100      
                    Track_Bar.Value = Typed_Value / Factor;


                    // Adjusting the Maximum Value of the Progress Bar and multiplicating x 100 to get the percentage
                    if (Progress_Bar != null)
                    {
                        Progress_Bar.Maximum = Maximal_Value;
                        Progress_Bar.Value = Typed_Value;
                    }
                } catch {}
            }
        }



        private void Track_Bar_Volume_Scroll(object sender, EventArgs e)
        {   if (Track_Bar_Volume.Value < 11)
            {
                Scroll_Xml_Value(Track_Bar_Volume, null, Text_Box_Max_Volume, 10);
                if (Track_Bar_Volume.Value > 0) { Text_Box_Min_Volume.Text = ((Track_Bar_Volume.Value - 1) * 10).ToString(); }
                Properties.Settings.Default.Volume = Track_Bar_Volume.Value;
                Properties.Settings.Default.Save();
            }      
        }

        private void Track_Bar_Pitch_Scroll(object sender, EventArgs e)
        {
            if (Track_Bar_Pitch.Value < 11)
            {
                Scroll_Xml_Value(Track_Bar_Pitch, null, Text_Box_Max_Pitch, 10);
                if (Track_Bar_Pitch.Value > 0) { Text_Box_Min_Pitch.Text = ((Track_Bar_Pitch.Value - 1) * 10).ToString(); }
                Properties.Settings.Default.Pitch = Track_Bar_Pitch.Value;
                Properties.Settings.Default.Save();
            }
        }

        private void Track_Bar_Probability_Scroll(object sender, EventArgs e)
        {
            if (Track_Bar_Probability.Value < 11)
            {
                Scroll_Xml_Value(Track_Bar_Probability, null, Text_Box_Max_Probability, 10);
                if (Track_Bar_Probability.Value > 0) { Text_Box_Min_Probability.Text = ((Track_Bar_Probability.Value - 1) * 10).ToString(); }
                Properties.Settings.Default.Delay = Track_Bar_Probability.Value;
                Properties.Settings.Default.Save();
            }   
        }


       

        private void Text_Box_Min_Volume_TextChanged(object sender, EventArgs e)
        {
            int Typed_Value = 0;
            Int32.TryParse(Text_Box_Min_Volume.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Volume, null, Typed_Value);

        }

        private void Text_Box_Max_Volume_TextChanged(object sender, EventArgs e)
        {   int Typed_Value = 0;
            Int32.TryParse(Text_Box_Max_Volume.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Volume, null, Typed_Value);
        
        }


        private void Text_Box_Min_Pitch_TextChanged(object sender, EventArgs e)
        {
            int Typed_Value = 0;
            Int32.TryParse(Text_Box_Min_Pitch.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Pitch, null, Typed_Value);

        }

        private void Text_Box_Max_Pitch_TextChanged(object sender, EventArgs e)
        {
            int Typed_Value = 0;
            Int32.TryParse(Text_Box_Max_Pitch.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Pitch, null, Typed_Value);

        }



        private void Text_Box_Min_Probability_TextChanged(object sender, EventArgs e)
        {
            int Typed_Value = 0;
            Int32.TryParse(Text_Box_Min_Probability.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Probability, null, Typed_Value);
        }

        private void Text_Box_Max_Probability_TextChanged(object sender, EventArgs e)
        {
            int Typed_Value = 0;
            Int32.TryParse(Text_Box_Max_Probability.Text, out Typed_Value);
            Text_Box_Text_Changed(Track_Bar_Probability, null, Typed_Value);
        }




        private void Button_Browse_Folder_Click(object sender, EventArgs e)
        {

        }

        private void Button_Xml_Folder_MouseHover(object sender, EventArgs e)
        {
            Hovering = true;
            Imp.Set_Resource_Button(Button_Xml_Folder, Properties.Resources.Button_Folder_Green_Lit);

            /*
            Temporal_C = 0;
            try { Temporal_C = Imp.Get_Selected_List_View_Items(List_View_Selection).Count; }  catch {}


            if (UI_Mode == "Search") { Imp.Set_Resource_Button(Button_Browse_Folder, Properties.Resources.Button_Save_Lit); }
            else if (UI_Mode == "Script" || UI_Mode == "Backup" && Temporal_C == 0)
            { Imp.Set_Resource_Button(Button_Browse_Folder, Properties.Resources.Button_Folder_Red_Lit); }
            else { Imp.Set_Resource_Button(Button_Browse_Folder, Properties.Resources.Button_Folder_Green_Lit); }


            if (Show_Tooltip)
            {
                string Tooltip_Name = "Open_Folder";
                if (UI_Mode == "Backup") { Tooltip_Name = "Open_Backup"; }

                Run_Tooltip(Tooltip_Name);
            }   
            */
        }

        private void Button_Xml_Folder_MouseLeave(object sender, EventArgs e)
        {   Hovering = false;
            Imp.Set_Resource_Button(Button_Xml_Folder, Properties.Resources.Button_Folder_Green);
        }

       

        private void Button_Xml_Folder_Click(object sender, EventArgs e)
        {

        }


        private void Button_Browse_Folder_MouseHover(object sender, EventArgs e)
        {
            Hovering = true;
            Imp.Set_Resource_Button(Button_Browse_Folder, Properties.Resources.Button_Folder_Red_Lit);
        }

        private void Button_Browse_Folder_MouseLeave(object sender, EventArgs e)
        {
            Hovering = false;
            Imp.Set_Resource_Button(Button_Browse_Folder, Properties.Resources.Button_Folder_Red);
        }



        private void Button_Dispatch_Click(object sender, EventArgs e)
        {
            if (Input_Folder == "") { return; }
            // File.WriteAllText(Output_Folder + "Structure.txt", string.Join("\n", Imp.Get_All_Directories(Output_Folder)));

            string[] Folder_Structure = new string[] 
            { "Ability_Activated", "Ability_Deactivated", "Ambient_Loop", "Ambient_Moving", "Assist_Attack", 
              "Assist_Move", "Asteroid_Damage", "Attack", "Attack_Hardpoint", "Barrage", "Bombard_Aim", "Bombard_Incoming", 
              "Build_Cancelled", "Build_Complete", "Build_Started", "Die", "Engine_Destroyed", "Engine_Idle", "Engine_Moving", 
              "Fire", "Fleet_Move", "Guard", "Health_Critical", "Health_Damaged", "Health_Low", "HP_Destroyed", "Into_Asteroids", 
              "Into_Nebula", "Move", "Select", "Shields_Destroyed", "Spawn", "Stop"
            };

            string Base = Input_Folder + @"Faction\Unit";

            if (!Directory.Exists(Base)) { Directory.CreateDirectory(Base); }
            // MessageBox.Show(Input_Folder + @"Faction\Unit");

            foreach (string Folder_Name in Folder_Structure) 
            { if (!Directory.Exists(Base + @"\" + Folder_Name)) { Directory.CreateDirectory(Base + @"\" + Folder_Name); }  }

        }

        private void Button_Dispatch_MouseHover(object sender, EventArgs e)
        {
            Hovering = true;
            Imp.Set_Resource_Button(Button_Dispatch, Properties.Resources.Button_Plus_Lit);
        }

        private void Button_Dispatch_MouseLeave(object sender, EventArgs e)
        {
            Hovering = false;
            Imp.Set_Resource_Button(Button_Dispatch, Properties.Resources.Button_Plus);
        }






    }
}
