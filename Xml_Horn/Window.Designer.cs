﻿namespace Xml_Horn
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Dispatch = new System.Windows.Forms.PictureBox();
            this.Text_Box_Folder_Path = new System.Windows.Forms.TextBox();
            this.Drop_Zone = new System.Windows.Forms.PictureBox();
            this.Button_Browse_Folder = new System.Windows.Forms.PictureBox();
            this.Button_Run = new System.Windows.Forms.PictureBox();
            this.Text_Box_Description = new System.Windows.Forms.RichTextBox();
            this.Text_Box_Output_Path = new System.Windows.Forms.TextBox();
            this.Button_Xml_Folder = new System.Windows.Forms.PictureBox();
            this.Label_Wav_Folder = new System.Windows.Forms.Label();
            this.Label_Output_Folder = new System.Windows.Forms.Label();
            this.Check_Box_Merge_Code = new System.Windows.Forms.CheckBox();
            this.Check_Box_Add_To_File = new System.Windows.Forms.CheckBox();
            this.Label_Presets = new System.Windows.Forms.Label();
            this.Combo_Box_Presets = new System.Windows.Forms.ComboBox();
            this.Text_Box_Text_ID = new System.Windows.Forms.RichTextBox();
            this.Check_Box_Localize = new System.Windows.Forms.CheckBox();
            this.Check_Box_Sequencially = new System.Windows.Forms.CheckBox();
            this.Track_Bar_Probability = new System.Windows.Forms.TrackBar();
            this.Text_Box_Chained_Event = new System.Windows.Forms.TextBox();
            this.Label_Chained_Event = new System.Windows.Forms.Label();
            this.Label_Probability = new System.Windows.Forms.Label();
            this.Text_Box_Max_Probability = new System.Windows.Forms.TextBox();
            this.Label_Path_Prefix = new System.Windows.Forms.Label();
            this.Combo_Box_Language = new System.Windows.Forms.ComboBox();
            this.Label_Language = new System.Windows.Forms.Label();
            this.Combo_Box_Path_Prefix = new System.Windows.Forms.ComboBox();
            this.Text_Box_Max_Pitch = new System.Windows.Forms.TextBox();
            this.Label_Pitch = new System.Windows.Forms.Label();
            this.Track_Bar_Pitch = new System.Windows.Forms.TrackBar();
            this.Text_Box_Min_Probability = new System.Windows.Forms.TextBox();
            this.Text_Box_Min_Pitch = new System.Windows.Forms.TextBox();
            this.Text_Box_Min_Volume = new System.Windows.Forms.TextBox();
            this.Text_Box_Max_Volume = new System.Windows.Forms.TextBox();
            this.Label_Volume = new System.Windows.Forms.Label();
            this.Track_Bar_Volume = new System.Windows.Forms.TrackBar();
            this.Check_Box_Overlap_Test = new System.Windows.Forms.CheckBox();
            this.List_View_Selection = new System.Windows.Forms.ListView();
            this.Entries = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.Button_Dispatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drop_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Browse_Folder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Run)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Xml_Folder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Probability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Pitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Dispatch
            // 
            this.Button_Dispatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Dispatch.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button_Dispatch.Location = new System.Drawing.Point(399, 69);
            this.Button_Dispatch.Name = "Button_Dispatch";
            this.Button_Dispatch.Size = new System.Drawing.Size(30, 30);
            this.Button_Dispatch.TabIndex = 41;
            this.Button_Dispatch.TabStop = false;
            this.Button_Dispatch.Click += new System.EventHandler(this.Button_Dispatch_Click);
            this.Button_Dispatch.MouseLeave += new System.EventHandler(this.Button_Dispatch_MouseLeave);
            this.Button_Dispatch.MouseHover += new System.EventHandler(this.Button_Dispatch_MouseHover);
            // 
            // Text_Box_Folder_Path
            // 
            this.Text_Box_Folder_Path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_Box_Folder_Path.BackColor = System.Drawing.Color.CadetBlue;
            this.Text_Box_Folder_Path.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Folder_Path.ForeColor = System.Drawing.SystemColors.Window;
            this.Text_Box_Folder_Path.Location = new System.Drawing.Point(31, 71);
            this.Text_Box_Folder_Path.Name = "Text_Box_Folder_Path";
            this.Text_Box_Folder_Path.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text_Box_Folder_Path.Size = new System.Drawing.Size(367, 26);
            this.Text_Box_Folder_Path.TabIndex = 40;
            this.Text_Box_Folder_Path.TextChanged += new System.EventHandler(this.Text_Box_Folder_Path_TextChanged);
            this.Text_Box_Folder_Path.DragDrop += new System.Windows.Forms.DragEventHandler(this.Text_Box_Folder_Path_DragDrop);
            this.Text_Box_Folder_Path.DragEnter += new System.Windows.Forms.DragEventHandler(this.Text_Box_Folder_Path_DragEnter);
            this.Text_Box_Folder_Path.DragOver += new System.Windows.Forms.DragEventHandler(this.Text_Box_Folder_Path_DragOver);
            // 
            // Drop_Zone
            // 
            this.Drop_Zone.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Drop_Zone.Location = new System.Drawing.Point(-1, -1);
            this.Drop_Zone.Name = "Drop_Zone";
            this.Drop_Zone.Size = new System.Drawing.Size(430, 190);
            this.Drop_Zone.TabIndex = 39;
            this.Drop_Zone.TabStop = false;
            this.Drop_Zone.Visible = false;
            this.Drop_Zone.Click += new System.EventHandler(this.Drop_Zone_Click);
            this.Drop_Zone.DragDrop += new System.Windows.Forms.DragEventHandler(this.Drop_Zone_DragDrop);
            this.Drop_Zone.DragEnter += new System.Windows.Forms.DragEventHandler(this.Drop_Zone_DragEnter);
            this.Drop_Zone.DragOver += new System.Windows.Forms.DragEventHandler(this.Drop_Zone_DragOver);
            // 
            // Button_Browse_Folder
            // 
            this.Button_Browse_Folder.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button_Browse_Folder.Location = new System.Drawing.Point(1, 69);
            this.Button_Browse_Folder.Name = "Button_Browse_Folder";
            this.Button_Browse_Folder.Size = new System.Drawing.Size(30, 30);
            this.Button_Browse_Folder.TabIndex = 44;
            this.Button_Browse_Folder.TabStop = false;
            this.Button_Browse_Folder.Click += new System.EventHandler(this.Button_Browse_Folder_Click);
            this.Button_Browse_Folder.MouseLeave += new System.EventHandler(this.Button_Browse_Folder_MouseLeave);
            this.Button_Browse_Folder.MouseHover += new System.EventHandler(this.Button_Browse_Folder_MouseHover);
            // 
            // Button_Run
            // 
            this.Button_Run.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Run.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button_Run.Location = new System.Drawing.Point(399, 139);
            this.Button_Run.Name = "Button_Run";
            this.Button_Run.Size = new System.Drawing.Size(30, 30);
            this.Button_Run.TabIndex = 45;
            this.Button_Run.TabStop = false;
            this.Button_Run.Click += new System.EventHandler(this.Button_Run_Click);
            // 
            // Text_Box_Description
            // 
            this.Text_Box_Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_Box_Description.BackColor = System.Drawing.SystemColors.MenuText;
            this.Text_Box_Description.Font = new System.Drawing.Font("Georgia", 15F);
            this.Text_Box_Description.ForeColor = System.Drawing.SystemColors.Info;
            this.Text_Box_Description.Location = new System.Drawing.Point(31, 12);
            this.Text_Box_Description.Name = "Text_Box_Description";
            this.Text_Box_Description.Size = new System.Drawing.Size(367, 164);
            this.Text_Box_Description.TabIndex = 42;
            this.Text_Box_Description.Text = "";
            this.Text_Box_Description.Visible = false;
            this.Text_Box_Description.Click += new System.EventHandler(this.Text_Box_Description_Click);
            // 
            // Text_Box_Output_Path
            // 
            this.Text_Box_Output_Path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_Box_Output_Path.BackColor = System.Drawing.Color.CadetBlue;
            this.Text_Box_Output_Path.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Output_Path.ForeColor = System.Drawing.SystemColors.Window;
            this.Text_Box_Output_Path.Location = new System.Drawing.Point(31, 141);
            this.Text_Box_Output_Path.Name = "Text_Box_Output_Path";
            this.Text_Box_Output_Path.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text_Box_Output_Path.Size = new System.Drawing.Size(367, 26);
            this.Text_Box_Output_Path.TabIndex = 46;
            this.Text_Box_Output_Path.DragDrop += new System.Windows.Forms.DragEventHandler(this.Text_Box_Output_Path_DragDrop);
            this.Text_Box_Output_Path.DragEnter += new System.Windows.Forms.DragEventHandler(this.Text_Box_Output_Path_DragEnter);
            this.Text_Box_Output_Path.DragOver += new System.Windows.Forms.DragEventHandler(this.Text_Box_Output_Path_DragOver);
            // 
            // Button_Xml_Folder
            // 
            this.Button_Xml_Folder.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button_Xml_Folder.Location = new System.Drawing.Point(1, 139);
            this.Button_Xml_Folder.Name = "Button_Xml_Folder";
            this.Button_Xml_Folder.Size = new System.Drawing.Size(30, 30);
            this.Button_Xml_Folder.TabIndex = 47;
            this.Button_Xml_Folder.TabStop = false;
            this.Button_Xml_Folder.Click += new System.EventHandler(this.Button_Xml_Folder_Click);
            this.Button_Xml_Folder.MouseLeave += new System.EventHandler(this.Button_Xml_Folder_MouseLeave);
            this.Button_Xml_Folder.MouseHover += new System.EventHandler(this.Button_Xml_Folder_MouseHover);
            // 
            // Label_Wav_Folder
            // 
            this.Label_Wav_Folder.AutoSize = true;
            this.Label_Wav_Folder.Font = new System.Drawing.Font("Georgia", 20F);
            this.Label_Wav_Folder.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Wav_Folder.Location = new System.Drawing.Point(31, 40);
            this.Label_Wav_Folder.Name = "Label_Wav_Folder";
            this.Label_Wav_Folder.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Wav_Folder.Size = new System.Drawing.Size(206, 31);
            this.Label_Wav_Folder.TabIndex = 48;
            this.Label_Wav_Folder.Text = "Wav Cue Folder";
            // 
            // Label_Output_Folder
            // 
            this.Label_Output_Folder.AutoSize = true;
            this.Label_Output_Folder.Font = new System.Drawing.Font("Georgia", 20F);
            this.Label_Output_Folder.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Output_Folder.Location = new System.Drawing.Point(31, 109);
            this.Label_Output_Folder.Name = "Label_Output_Folder";
            this.Label_Output_Folder.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Output_Folder.Size = new System.Drawing.Size(243, 31);
            this.Label_Output_Folder.TabIndex = 49;
            this.Label_Output_Folder.Text = "Xml Output Folder";
            // 
            // Check_Box_Merge_Code
            // 
            this.Check_Box_Merge_Code.AutoSize = true;
            this.Check_Box_Merge_Code.Font = new System.Drawing.Font("Georgia", 12F);
            this.Check_Box_Merge_Code.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Check_Box_Merge_Code.Location = new System.Drawing.Point(224, 351);
            this.Check_Box_Merge_Code.Name = "Check_Box_Merge_Code";
            this.Check_Box_Merge_Code.Size = new System.Drawing.Size(177, 22);
            this.Check_Box_Merge_Code.TabIndex = 50;
            this.Check_Box_Merge_Code.Text = "Merge code into 1 file";
            this.Check_Box_Merge_Code.UseVisualStyleBackColor = true;
            this.Check_Box_Merge_Code.CheckedChanged += new System.EventHandler(this.Check_Box_Merge_Code_CheckedChanged);
            // 
            // Check_Box_Add_To_File
            // 
            this.Check_Box_Add_To_File.AutoSize = true;
            this.Check_Box_Add_To_File.Font = new System.Drawing.Font("Georgia", 12F);
            this.Check_Box_Add_To_File.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Check_Box_Add_To_File.Location = new System.Drawing.Point(224, 331);
            this.Check_Box_Add_To_File.Name = "Check_Box_Add_To_File";
            this.Check_Box_Add_To_File.Size = new System.Drawing.Size(183, 22);
            this.Check_Box_Add_To_File.TabIndex = 51;
            this.Check_Box_Add_To_File.Text = "Add to SFXEventFiles";
            this.Check_Box_Add_To_File.UseVisualStyleBackColor = true;
            this.Check_Box_Add_To_File.CheckedChanged += new System.EventHandler(this.Check_Box_Add_To_File_CheckedChanged);
            // 
            // Label_Presets
            // 
            this.Label_Presets.AutoSize = true;
            this.Label_Presets.Font = new System.Drawing.Font("Georgia", 20F);
            this.Label_Presets.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Presets.Location = new System.Drawing.Point(31, 249);
            this.Label_Presets.Name = "Label_Presets";
            this.Label_Presets.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Presets.Size = new System.Drawing.Size(100, 31);
            this.Label_Presets.TabIndex = 52;
            this.Label_Presets.Text = "Presets";
            // 
            // Combo_Box_Presets
            // 
            this.Combo_Box_Presets.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Combo_Box_Presets.Font = new System.Drawing.Font("Georgia", 12F);
            this.Combo_Box_Presets.FormattingEnabled = true;
            this.Combo_Box_Presets.Items.AddRange(new object[] {
            "Space",
            "Ground",
            "Buildings",
            "",
            "Preset_UR",
            "Preset_GUNV",
            "",
            "Preset_EXP",
            "Preset_EXP_SR",
            "Preset_EXP_LR",
            "",
            "Preset_GUI",
            "Preset_HUD",
            "",
            "Preset_BDS",
            "Preset_GUNS",
            "Preset_EXPC",
            "Preset_EGLC_Idle",
            "Preset_EGB_Land"});
            this.Combo_Box_Presets.Location = new System.Drawing.Point(31, 281);
            this.Combo_Box_Presets.Name = "Combo_Box_Presets";
            this.Combo_Box_Presets.Size = new System.Drawing.Size(125, 26);
            this.Combo_Box_Presets.TabIndex = 55;
            this.Combo_Box_Presets.SelectedIndexChanged += new System.EventHandler(this.Combo_Box_Presets_SelectedIndexChanged);
            // 
            // Text_Box_Text_ID
            // 
            this.Text_Box_Text_ID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_Box_Text_ID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Text_ID.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Text_ID.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Text_Box_Text_ID.Location = new System.Drawing.Point(31, 211);
            this.Text_Box_Text_ID.Name = "Text_Box_Text_ID";
            this.Text_Box_Text_ID.Size = new System.Drawing.Size(367, 96);
            this.Text_Box_Text_ID.TabIndex = 58;
            this.Text_Box_Text_ID.Text = "";
            this.Text_Box_Text_ID.Visible = false;
            // 
            // Check_Box_Localize
            // 
            this.Check_Box_Localize.AutoSize = true;
            this.Check_Box_Localize.Font = new System.Drawing.Font("Georgia", 12F);
            this.Check_Box_Localize.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Check_Box_Localize.Location = new System.Drawing.Point(31, 331);
            this.Check_Box_Localize.Name = "Check_Box_Localize";
            this.Check_Box_Localize.Size = new System.Drawing.Size(83, 22);
            this.Check_Box_Localize.TabIndex = 60;
            this.Check_Box_Localize.Text = "Localize";
            this.Check_Box_Localize.UseVisualStyleBackColor = true;
            this.Check_Box_Localize.CheckedChanged += new System.EventHandler(this.Check_Box_Localize_CheckedChanged);
            // 
            // Check_Box_Sequencially
            // 
            this.Check_Box_Sequencially.AutoSize = true;
            this.Check_Box_Sequencially.Font = new System.Drawing.Font("Georgia", 12F);
            this.Check_Box_Sequencially.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Check_Box_Sequencially.Location = new System.Drawing.Point(31, 351);
            this.Check_Box_Sequencially.Name = "Check_Box_Sequencially";
            this.Check_Box_Sequencially.Size = new System.Drawing.Size(150, 22);
            this.Check_Box_Sequencially.TabIndex = 59;
            this.Check_Box_Sequencially.Text = "Play Sequencially";
            this.Check_Box_Sequencially.UseVisualStyleBackColor = true;
            this.Check_Box_Sequencially.CheckedChanged += new System.EventHandler(this.Check_Box_Sequencially_CheckedChanged);
            // 
            // Track_Bar_Probability
            // 
            this.Track_Bar_Probability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Track_Bar_Probability.LargeChange = 1;
            this.Track_Bar_Probability.Location = new System.Drawing.Point(24, 586);
            this.Track_Bar_Probability.Name = "Track_Bar_Probability";
            this.Track_Bar_Probability.Size = new System.Drawing.Size(381, 45);
            this.Track_Bar_Probability.TabIndex = 61;
            this.Track_Bar_Probability.Scroll += new System.EventHandler(this.Track_Bar_Probability_Scroll);
            // 
            // Text_Box_Chained_Event
            // 
            this.Text_Box_Chained_Event.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Chained_Event.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Chained_Event.Location = new System.Drawing.Point(163, 281);
            this.Text_Box_Chained_Event.Name = "Text_Box_Chained_Event";
            this.Text_Box_Chained_Event.Size = new System.Drawing.Size(235, 26);
            this.Text_Box_Chained_Event.TabIndex = 62;
            this.Text_Box_Chained_Event.TextChanged += new System.EventHandler(this.Text_Box_Chained_Event_TextChanged);
            // 
            // Label_Chained_Event
            // 
            this.Label_Chained_Event.AutoSize = true;
            this.Label_Chained_Event.Font = new System.Drawing.Font("Georgia", 16F);
            this.Label_Chained_Event.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Chained_Event.Location = new System.Drawing.Point(174, 254);
            this.Label_Chained_Event.Name = "Label_Chained_Event";
            this.Label_Chained_Event.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Chained_Event.Size = new System.Drawing.Size(201, 27);
            this.Label_Chained_Event.TabIndex = 63;
            this.Label_Chained_Event.Text = "Chained SFX Event";
            // 
            // Label_Probability
            // 
            this.Label_Probability.AutoSize = true;
            this.Label_Probability.Font = new System.Drawing.Font("Georgia", 18F);
            this.Label_Probability.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Probability.Location = new System.Drawing.Point(31, 556);
            this.Label_Probability.Name = "Label_Probability";
            this.Label_Probability.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Probability.Size = new System.Drawing.Size(130, 29);
            this.Label_Probability.TabIndex = 64;
            this.Label_Probability.Text = "Probability";
            // 
            // Text_Box_Max_Probability
            // 
            this.Text_Box_Max_Probability.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Max_Probability.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Max_Probability.Location = new System.Drawing.Point(358, 558);
            this.Text_Box_Max_Probability.Name = "Text_Box_Max_Probability";
            this.Text_Box_Max_Probability.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Max_Probability.TabIndex = 65;
            this.Text_Box_Max_Probability.TextChanged += new System.EventHandler(this.Text_Box_Max_Probability_TextChanged);
            // 
            // Label_Path_Prefix
            // 
            this.Label_Path_Prefix.AutoSize = true;
            this.Label_Path_Prefix.Font = new System.Drawing.Font("Georgia", 16F);
            this.Label_Path_Prefix.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Path_Prefix.Location = new System.Drawing.Point(174, 184);
            this.Label_Path_Prefix.Name = "Label_Path_Prefix";
            this.Label_Path_Prefix.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Path_Prefix.Size = new System.Drawing.Size(119, 27);
            this.Label_Path_Prefix.TabIndex = 69;
            this.Label_Path_Prefix.Text = "Path Prefix";
            // 
            // Combo_Box_Language
            // 
            this.Combo_Box_Language.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Combo_Box_Language.Font = new System.Drawing.Font("Georgia", 12F);
            this.Combo_Box_Language.FormattingEnabled = true;
            this.Combo_Box_Language.Items.AddRange(new object[] {
            "Eng",
            "Ger",
            "Fr"});
            this.Combo_Box_Language.Location = new System.Drawing.Point(31, 211);
            this.Combo_Box_Language.Name = "Combo_Box_Language";
            this.Combo_Box_Language.Size = new System.Drawing.Size(125, 26);
            this.Combo_Box_Language.TabIndex = 67;
            this.Combo_Box_Language.TextChanged += new System.EventHandler(this.Combo_Box_Language_TextChanged);
            // 
            // Label_Language
            // 
            this.Label_Language.AutoSize = true;
            this.Label_Language.Font = new System.Drawing.Font("Georgia", 20F);
            this.Label_Language.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Language.Location = new System.Drawing.Point(31, 179);
            this.Label_Language.Name = "Label_Language";
            this.Label_Language.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Language.Size = new System.Drawing.Size(131, 31);
            this.Label_Language.TabIndex = 66;
            this.Label_Language.Text = "Language";
            // 
            // Combo_Box_Path_Prefix
            // 
            this.Combo_Box_Path_Prefix.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Combo_Box_Path_Prefix.Font = new System.Drawing.Font("Georgia", 12F);
            this.Combo_Box_Path_Prefix.FormattingEnabled = true;
            this.Combo_Box_Path_Prefix.Items.AddRange(new object[] {
            "data\\audio\\"});
            this.Combo_Box_Path_Prefix.Location = new System.Drawing.Point(163, 211);
            this.Combo_Box_Path_Prefix.Name = "Combo_Box_Path_Prefix";
            this.Combo_Box_Path_Prefix.Size = new System.Drawing.Size(235, 26);
            this.Combo_Box_Path_Prefix.TabIndex = 70;
            this.Combo_Box_Path_Prefix.TextChanged += new System.EventHandler(this.Combo_Box_Path_Prefix_TextChanged);
            // 
            // Text_Box_Max_Pitch
            // 
            this.Text_Box_Max_Pitch.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Max_Pitch.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Max_Pitch.Location = new System.Drawing.Point(358, 488);
            this.Text_Box_Max_Pitch.Name = "Text_Box_Max_Pitch";
            this.Text_Box_Max_Pitch.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Max_Pitch.TabIndex = 73;
            this.Text_Box_Max_Pitch.TextChanged += new System.EventHandler(this.Text_Box_Max_Pitch_TextChanged);
            // 
            // Label_Pitch
            // 
            this.Label_Pitch.AutoSize = true;
            this.Label_Pitch.Font = new System.Drawing.Font("Georgia", 18F);
            this.Label_Pitch.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Pitch.Location = new System.Drawing.Point(31, 486);
            this.Label_Pitch.Name = "Label_Pitch";
            this.Label_Pitch.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Pitch.Size = new System.Drawing.Size(68, 29);
            this.Label_Pitch.TabIndex = 72;
            this.Label_Pitch.Text = "Pitch";
            // 
            // Track_Bar_Pitch
            // 
            this.Track_Bar_Pitch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Track_Bar_Pitch.LargeChange = 1;
            this.Track_Bar_Pitch.Location = new System.Drawing.Point(24, 516);
            this.Track_Bar_Pitch.Name = "Track_Bar_Pitch";
            this.Track_Bar_Pitch.Size = new System.Drawing.Size(381, 45);
            this.Track_Bar_Pitch.TabIndex = 71;
            this.Track_Bar_Pitch.Scroll += new System.EventHandler(this.Track_Bar_Pitch_Scroll);
            // 
            // Text_Box_Min_Probability
            // 
            this.Text_Box_Min_Probability.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Min_Probability.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Min_Probability.Location = new System.Drawing.Point(314, 558);
            this.Text_Box_Min_Probability.Name = "Text_Box_Min_Probability";
            this.Text_Box_Min_Probability.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Min_Probability.TabIndex = 74;
            this.Text_Box_Min_Probability.Visible = false;
            this.Text_Box_Min_Probability.TextChanged += new System.EventHandler(this.Text_Box_Min_Probability_TextChanged);
            // 
            // Text_Box_Min_Pitch
            // 
            this.Text_Box_Min_Pitch.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Min_Pitch.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Min_Pitch.Location = new System.Drawing.Point(314, 488);
            this.Text_Box_Min_Pitch.Name = "Text_Box_Min_Pitch";
            this.Text_Box_Min_Pitch.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Min_Pitch.TabIndex = 75;
            this.Text_Box_Min_Pitch.TextChanged += new System.EventHandler(this.Text_Box_Min_Pitch_TextChanged);
            // 
            // Text_Box_Min_Volume
            // 
            this.Text_Box_Min_Volume.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Min_Volume.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Min_Volume.Location = new System.Drawing.Point(314, 412);
            this.Text_Box_Min_Volume.Name = "Text_Box_Min_Volume";
            this.Text_Box_Min_Volume.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Min_Volume.TabIndex = 79;
            this.Text_Box_Min_Volume.TextChanged += new System.EventHandler(this.Text_Box_Min_Volume_TextChanged);
            // 
            // Text_Box_Max_Volume
            // 
            this.Text_Box_Max_Volume.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Text_Box_Max_Volume.Font = new System.Drawing.Font("Georgia", 12F);
            this.Text_Box_Max_Volume.Location = new System.Drawing.Point(358, 412);
            this.Text_Box_Max_Volume.Name = "Text_Box_Max_Volume";
            this.Text_Box_Max_Volume.Size = new System.Drawing.Size(38, 26);
            this.Text_Box_Max_Volume.TabIndex = 78;
            this.Text_Box_Max_Volume.TextChanged += new System.EventHandler(this.Text_Box_Max_Volume_TextChanged);
            // 
            // Label_Volume
            // 
            this.Label_Volume.AutoSize = true;
            this.Label_Volume.Font = new System.Drawing.Font("Georgia", 18F);
            this.Label_Volume.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label_Volume.Location = new System.Drawing.Point(31, 410);
            this.Label_Volume.Name = "Label_Volume";
            this.Label_Volume.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label_Volume.Size = new System.Drawing.Size(96, 29);
            this.Label_Volume.TabIndex = 77;
            this.Label_Volume.Text = "Volume";
            // 
            // Track_Bar_Volume
            // 
            this.Track_Bar_Volume.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Track_Bar_Volume.LargeChange = 1;
            this.Track_Bar_Volume.Location = new System.Drawing.Point(24, 440);
            this.Track_Bar_Volume.Name = "Track_Bar_Volume";
            this.Track_Bar_Volume.Size = new System.Drawing.Size(381, 45);
            this.Track_Bar_Volume.TabIndex = 76;
            this.Track_Bar_Volume.Scroll += new System.EventHandler(this.Track_Bar_Volume_Scroll);
            // 
            // Check_Box_Overlap_Test
            // 
            this.Check_Box_Overlap_Test.AutoSize = true;
            this.Check_Box_Overlap_Test.Font = new System.Drawing.Font("Georgia", 12F);
            this.Check_Box_Overlap_Test.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Check_Box_Overlap_Test.Location = new System.Drawing.Point(31, 371);
            this.Check_Box_Overlap_Test.Name = "Check_Box_Overlap_Test";
            this.Check_Box_Overlap_Test.Size = new System.Drawing.Size(120, 22);
            this.Check_Box_Overlap_Test.TabIndex = 80;
            this.Check_Box_Overlap_Test.Text = "Overlap Test";
            this.Check_Box_Overlap_Test.UseVisualStyleBackColor = true;
            this.Check_Box_Overlap_Test.CheckedChanged += new System.EventHandler(this.Check_Box_Overlap_Test_CheckedChanged);
            // 
            // List_View_Selection
            // 
            this.List_View_Selection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.List_View_Selection.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.List_View_Selection.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Entries});
            this.List_View_Selection.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.List_View_Selection.FullRowSelect = true;
            this.List_View_Selection.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.List_View_Selection.Location = new System.Drawing.Point(31, 12);
            this.List_View_Selection.Name = "List_View_Selection";
            this.List_View_Selection.Size = new System.Drawing.Size(367, 164);
            this.List_View_Selection.TabIndex = 81;
            this.List_View_Selection.UseCompatibleStateImageBehavior = false;
            this.List_View_Selection.View = System.Windows.Forms.View.Details;
            this.List_View_Selection.Visible = false;
            // 
            // Entries
            // 
            this.Entries.Width = 396;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(428, 622);
            this.Controls.Add(this.Check_Box_Overlap_Test);
            this.Controls.Add(this.Text_Box_Min_Volume);
            this.Controls.Add(this.Text_Box_Max_Volume);
            this.Controls.Add(this.Label_Volume);
            this.Controls.Add(this.Track_Bar_Volume);
            this.Controls.Add(this.Text_Box_Min_Probability);
            this.Controls.Add(this.Text_Box_Min_Pitch);
            this.Controls.Add(this.Text_Box_Max_Probability);
            this.Controls.Add(this.Text_Box_Max_Pitch);
            this.Controls.Add(this.Label_Pitch);
            this.Controls.Add(this.Track_Bar_Pitch);
            this.Controls.Add(this.Combo_Box_Path_Prefix);
            this.Controls.Add(this.Label_Path_Prefix);
            this.Controls.Add(this.Combo_Box_Language);
            this.Controls.Add(this.Label_Language);
            this.Controls.Add(this.Label_Probability);
            this.Controls.Add(this.Label_Chained_Event);
            this.Controls.Add(this.Text_Box_Chained_Event);
            this.Controls.Add(this.Track_Bar_Probability);
            this.Controls.Add(this.Check_Box_Localize);
            this.Controls.Add(this.Check_Box_Sequencially);
            this.Controls.Add(this.Combo_Box_Presets);
            this.Controls.Add(this.Label_Presets);
            this.Controls.Add(this.Check_Box_Add_To_File);
            this.Controls.Add(this.Check_Box_Merge_Code);
            this.Controls.Add(this.Label_Output_Folder);
            this.Controls.Add(this.Label_Wav_Folder);
            this.Controls.Add(this.Text_Box_Output_Path);
            this.Controls.Add(this.Button_Xml_Folder);
            this.Controls.Add(this.Button_Run);
            this.Controls.Add(this.Button_Dispatch);
            this.Controls.Add(this.Text_Box_Folder_Path);
            this.Controls.Add(this.Button_Browse_Folder);
            this.Controls.Add(this.Text_Box_Text_ID);
            this.Controls.Add(this.List_View_Selection);
            this.Controls.Add(this.Drop_Zone);
            this.Controls.Add(this.Text_Box_Description);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MinimumSize = new System.Drawing.Size(444, 660);
            this.Name = "Window";
            this.Text = "     Imperialware                          Xml Horn";
            ((System.ComponentModel.ISupportInitialize)(this.Button_Dispatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drop_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Browse_Folder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Run)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Xml_Folder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Probability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Pitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_Bar_Volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Button_Dispatch;
        private System.Windows.Forms.TextBox Text_Box_Folder_Path;
        private System.Windows.Forms.PictureBox Drop_Zone;
        private System.Windows.Forms.PictureBox Button_Browse_Folder;
        private System.Windows.Forms.PictureBox Button_Run;
        private System.Windows.Forms.RichTextBox Text_Box_Description;
        private System.Windows.Forms.TextBox Text_Box_Output_Path;
        private System.Windows.Forms.PictureBox Button_Xml_Folder;
        private System.Windows.Forms.Label Label_Wav_Folder;
        private System.Windows.Forms.Label Label_Output_Folder;
        private System.Windows.Forms.CheckBox Check_Box_Merge_Code;
        private System.Windows.Forms.CheckBox Check_Box_Add_To_File;
        private System.Windows.Forms.Label Label_Presets;
        private System.Windows.Forms.ComboBox Combo_Box_Presets;
        private System.Windows.Forms.RichTextBox Text_Box_Text_ID;
        private System.Windows.Forms.CheckBox Check_Box_Localize;
        private System.Windows.Forms.CheckBox Check_Box_Sequencially;
        private System.Windows.Forms.TrackBar Track_Bar_Probability;
        private System.Windows.Forms.TextBox Text_Box_Chained_Event;
        private System.Windows.Forms.Label Label_Chained_Event;
        private System.Windows.Forms.Label Label_Probability;
        private System.Windows.Forms.TextBox Text_Box_Max_Probability;
        private System.Windows.Forms.Label Label_Path_Prefix;
        private System.Windows.Forms.ComboBox Combo_Box_Language;
        private System.Windows.Forms.Label Label_Language;
        private System.Windows.Forms.ComboBox Combo_Box_Path_Prefix;
        private System.Windows.Forms.TextBox Text_Box_Max_Pitch;
        private System.Windows.Forms.Label Label_Pitch;
        private System.Windows.Forms.TrackBar Track_Bar_Pitch;
        private System.Windows.Forms.TextBox Text_Box_Min_Probability;
        private System.Windows.Forms.TextBox Text_Box_Min_Pitch;
        private System.Windows.Forms.TextBox Text_Box_Min_Volume;
        private System.Windows.Forms.TextBox Text_Box_Max_Volume;
        private System.Windows.Forms.Label Label_Volume;
        private System.Windows.Forms.TrackBar Track_Bar_Volume;
        private System.Windows.Forms.CheckBox Check_Box_Overlap_Test;
        private System.Windows.Forms.ListView List_View_Selection;
        private System.Windows.Forms.ColumnHeader Entries;
    }
}

